#!/usr/bin/env python
import socket
import random


if __name__ == "__main__":

    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # AF_INET - IPv4
    # SOCK_STREAM - TCP
    mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # SO_REUSEADDR - evita el colapso de la conexión
    mySocket.bind(('localhost', 1237)) #if localhost is empty, it will listen on all interfaces
    mySocket.listen(5)# 5 - max connections

    while True:# True - infinity loop
        print('waiting for connection...')
        connectionSocket, addr = mySocket.accept()# accept() - wait for connection  addr - address of connected client
        print('connected to: '+ str(addr))
        respuesta_aleatorio = "HTTP/1.1 307 Temporary Redirect\r\n" \
                              + 'Location: http://localhost:1237/' + str(random.randint(1, 1000)) +"\r\n" \
                              + "\r\n"

        recibido = connectionSocket.recv(2048)# 2048 - max bytes to receive
        print(recibido)
        connectionSocket.send(respuesta_aleatorio.encode('utf-8'))# encode - convert string to bytes
        connectionSocket.close()#
