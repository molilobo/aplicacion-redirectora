# Aplicación Redirectora

Repositorio de plantilla para el ejercicio "Aplicación Redirectora". Recuerda que puedes consultar su enunciado en la guía de estudio (programa) de la asignatura.

Construir un programa en Python que sirva cualquier invocacion que se le
realice con una redirecci´on (c´odigos de resultado HTTP en el rango 3xx) a otro
recurso (aleatorio), que bien puede ser de sı mismo (como en el ejercicio 17.3) o
externo a partir de una lista con URLs.

## Comentarios:
Para poder observar con mas facilidad en el navegador lo que esta ocurriendo,
confgura el panel de Red de forma que mantenga el log de transacciones de forma
permanente (en lugar de reiniciarlo cada vez que se pida una pagina nueva). En
Firefox, esto se hace en la configuraci´on del panel de Red, activando la opci´on
“Log persistente”.